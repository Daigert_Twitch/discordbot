import * as Discord from 'discord.js';
import discordConfig from '../../configs/discord-config.json';
import {
  ADD_COMMAND,
  DELETE_COMMAND,
  EDIT_COMMAND,
} from '../constants/constants';
import {
  addCommand,
  deleteCommand,
  editCommand,
  getCommandMessage,
} from '../com-manager/com-manager';
import {
  CIV_PLAY,
  civBanCommand,
} from '../com-manager/civban/civban';

const client = new Discord.Client();

export function loginToDiscord() {
  return client.login(discordConfig.token);
}

function sendMessageToChannel(channel, message) {
  channel.send(message);
}

export function init() {
  client.on('message', (message) => {
    if (message.author && message.author.bot) return;

    if (message.content.startsWith(ADD_COMMAND)) {
      try {
        addCommand(message.content).then(res => sendMessageToChannel(message.channel, res));
      } catch (error) {
        sendMessageToChannel(message.channel, error.message);
      }
    }

    if (message.content.startsWith(DELETE_COMMAND)) {
      deleteCommand(message.content).then(res => sendMessageToChannel(message.channel, res));
    }

    if (message.content.startsWith(EDIT_COMMAND)) {
      try {
        editCommand(message.content).then(res => sendMessageToChannel(message.channel, res));
      } catch (error) {
        sendMessageToChannel(message.channel, error.message);
      }
    }

    if (message.content.startsWith(CIV_PLAY)) {
      const players = civBanCommand(message.content);
      players.forEach(player => player.nations.length
        && message.channel.send(`${player.name} выбирает из: ${player.nations.join(', ')}.`));
    }

    if (message.content.startsWith('!')) {
      const commandMessage = getCommandMessage(message.content);
      if (commandMessage) sendMessageToChannel(message.channel, commandMessage);
    }
  });
}
