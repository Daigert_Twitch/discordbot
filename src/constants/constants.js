export const ADD_COMMAND = '!dbaddcom';
export const DELETE_COMMAND = '!dbdelcom';
export const EDIT_COMMAND = '!dbeditcom';

export const COMMAND_ERROR = {
  NO_EXCLAMATION_MARK: 'Где же восклицательный знак перед ":name" <:SMOrc:427509178374946818>',
  ALREADY_EXISTS: 'Команда ":name" уже существует',
  NO_SUCH_COMMAND: 'Команда ":name" не существует',
};

export const COMMAND_SUCCESS = {
  ADD: 'Команда ":name" была успешно добавлена.',
  EDIT: 'Команда ":name" была успешно обновлена.',
  DELETE: 'Команда ":name" была удалена.',
};
