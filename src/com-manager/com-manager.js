import {
  getCommandList,
  postNewCommand,
  removeCommand,
  updateCommand,
} from '../firebase/firebase';
import {
  ADD_COMMAND,
  COMMAND_ERROR,
  COMMAND_SUCCESS,
  DELETE_COMMAND, EDIT_COMMAND,
} from '../constants/constants';
import { matchCommand } from './helpers/helpers';

const commandNameMatch = ':name';
let commandErrorMsg;

function isNewCommandValid(name) {
  if (!name.startsWith('!')) {
    commandErrorMsg = COMMAND_ERROR.NO_EXCLAMATION_MARK.replace(commandNameMatch, name);
    return false;
  }

  const commandExists = getCommandList().some(command => command.name === name);
  if (commandExists) {
    commandErrorMsg = COMMAND_ERROR.ALREADY_EXISTS.replace(commandNameMatch, name);
    return false;
  }
  return true;
}

function isEditCommandValid(name) {
  if (!name.startsWith('!')) {
    commandErrorMsg = COMMAND_ERROR.NO_EXCLAMATION_MARK.replace(commandNameMatch, name);
    return false;
  }

  const commandExists = getCommandList().some(command => command.name === name);
  if (!commandExists) {
    commandErrorMsg = COMMAND_ERROR.NO_SUCH_COMMAND.replace(commandNameMatch, name);
    return false;
  }
  return true;
}

export function deleteCommand(msg) {
  const content = msg.replace(DELETE_COMMAND, '');
  const match = matchCommand(content);
  const { name } = match;
  return removeCommand(name).then(() => COMMAND_SUCCESS.DELETE.replace(commandNameMatch, name));
}


export function addCommand(msg) {
  const content = msg.replace(ADD_COMMAND, '');
  const match = matchCommand(content);
  const { name, message } = match;

  if (!isNewCommandValid(name)) {
    throw new Error(commandErrorMsg);
  }

  return postNewCommand(name, message).then(
    () => COMMAND_SUCCESS.ADD.replace(commandNameMatch, name),
  );
}

export function editCommand(msg) {
  const content = msg.replace(EDIT_COMMAND, '');
  const match = matchCommand(content);
  const { name, message } = match;

  if (!isEditCommandValid(name)) {
    throw new Error(commandErrorMsg);
  }

  return updateCommand(name, message).then(
    () => COMMAND_SUCCESS.EDIT.replace(commandNameMatch, name),
  );
}

export function getCommandMessage(content) {
  const command = getCommandList().find(com => content === com.name);
  if (!command) {
    return null;
  }
  return command.message;
}
