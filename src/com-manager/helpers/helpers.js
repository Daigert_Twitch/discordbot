// eslint-disable-next-line import/prefer-default-export
export function matchCommand(content) {
  const match = content.match(/([!\w\d]+)(.*)/);
  return {
    name: match[1],
    message: match[2],
  };
}
