import * as civban from './civban';

// TODO: fix jest.mock issues
// import { matchCommand } from '../helpers/helpers';
//
// jest.mock('../helpers/helpers', () => ({
//   matchCommand: jest.fn().mockReturnValue({
//     name: CIV_PLAY,
//     message: 'message',
//   }),
// }));

describe('civban', () => {
  test('should exist', () => {
    expect(civban).toEqual(expect.any(Object));
  });

  describe('civBanCommand', () => {
    test('should be a function', () => {
      expect(civban.civBanCommand).toBeInstanceOf(Function);
    });

    test('should return a string if command is valid', () => {
      const command = {
        content: `${civban.CIV_PLAY}`,
      };
      expect(civban.civBanCommand(command)).toEqual(expect.any(String));
    });

    test('should throw an error if command is invalid', () => {
      const command = '';
      try {
        civban.civBanCommand(command);
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });

  describe('validate', () => {
    test('should be a function', () => {
      expect(civban.isValid).toBeInstanceOf(Function);
    });

    test('should return true if command startsWith CIV_PLAY', () => {
      expect(civban.isValid(civban.CIV_PLAY)).toEqual(true);
    });

    test('should return false if command is not defined', () => {
      expect(civban.isValid()).toEqual(false);
    });

    test('should return false if command starts not with "!"', () => {
      const command = 'civ';
      expect(civban.isValid(command)).toEqual(false);
    });
  });

  describe('civPlayers', () => {
    describe('getCivPlayers', () => {
      test('should be function', () => {
        expect(civban.getCivPlayers).toBeInstanceOf(Function);
      });

      test('should return array players', () => {
        expect(civban.getCivPlayers()).toEqual([]);
      });
    });

    describe('setCivPlayers', () => {
      test('should be function', () => {
        expect(civban.setCivPlayers).toBeInstanceOf(Function);
      });

      test('should create an array player objects from string', () => {
        const players = 'Player1 Player2 Player3';
        civban.setCivPlayers(players);
        expect(civban.getCivPlayers()).toEqual([
          { name: 'Player1', nations: [] },
          { name: 'Player2', nations: [] },
          { name: 'Player3', nations: [] },
        ]);
      });
    });
  });
});
