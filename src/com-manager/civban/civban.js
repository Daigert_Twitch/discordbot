import { matchCommand } from '../helpers/helpers';
import { nations } from './civ-nations';

// TODO: MAKE IT BETTER !civ players; !civ ban
export const CIV_PLAY = '!civplay';

let availableNations;
let civPlayers = [];
let civBans = [];
let civPlayersWereSet = false;

const delimeter = /[\s,|]+/;

export function getCivPlayers() {
  return civPlayers;
}

export function setCivPlayers(players) {
  civPlayers = players.trim().split(delimeter).map(player => ({
    name: player,
    nations: [],
  }));
  civPlayersWereSet = true;
  console.log('civBanCommand:', '=>', 'Players were set', getCivPlayers());
}

function removeNationFromList(nation) {
  const nationIndex = availableNations.indexOf(nation);
  availableNations.splice(nationIndex, 1);
}

function setCivBans(bans) {
  availableNations = nations.slice();
  civBans = bans.split(' ');
}

export function isValid(command) {
  if (!command) return false;
  if (!command.startsWith(CIV_PLAY)) return false;
  return true;
}

function getNationsNumber() {
  return availableNations.length;
}

function getNationByIndex(index) {
  return availableNations[index];
}

function generateNationsPerPlayer() {
  let numberOfNationsPerPlayer = 4;
  civBans.forEach(removeNationFromList);
  while (numberOfNationsPerPlayer > 0) {
    civPlayers.forEach((player) => {
      const nationsNumber = getNationsNumber();
      const randomIndex = Math.floor(Math.random() * (nationsNumber - 1));
      player.nations.push(getNationByIndex(randomIndex));
      removeNationFromList(getNationByIndex(randomIndex));
    });
    numberOfNationsPerPlayer -= 1;
  }
}

export function civBanCommand(message) {
  console.log('civBanCommand:');
  const match = matchCommand(message);
  if (!isValid(match.name)) {
    throw new Error();
  }

  if (civPlayersWereSet) {
    setCivBans(match.message);
    generateNationsPerPlayer();
    civPlayersWereSet = false;
  } else {
    setCivPlayers(match.message);
  }
  return getCivPlayers();
}
