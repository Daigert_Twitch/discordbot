import * as Firebase from 'firebase';
import 'firebase/firestore';
import firebaseConfig from '../../configs/firebase-config.json';

Firebase.initializeApp(firebaseConfig);
const db = Firebase.firestore();
const commandsCollection = db.collection('commands');
let commandsList = [];

export function getCommandsCollection() {
  return commandsCollection.get();
}

export function setCommandsList() {
  commandsList = [];
  return getCommandsCollection().then(docs => docs.forEach(doc => commandsList.push(doc.data())));
}
export function postNewCommand(name, message) {
  return commandsCollection.doc(name).set({
    name,
    message,
  }).then(setCommandsList);
}

export function updateCommand(name, message) {
  return commandsCollection.doc(name).update({
    name,
    message,
  }).then(setCommandsList);
}

export function removeCommand(name) {
  return commandsCollection.doc(name).delete().then(setCommandsList);
}

export function getCommandList() {
  return commandsList;
}
