import { init as discordInit, loginToDiscord } from './discord/discord';
import { setCommandsList } from './firebase/firebase';

Promise.all([
  loginToDiscord(),
  setCommandsList(),
]).then(() => {
  discordInit();
});
